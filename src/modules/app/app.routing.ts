import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainPageComponent } from 'src/modules/app/components/main-page/main-page.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: MainPageComponent
    },
    {
        path: 'z2h',
        children: [
            {
                path: 'books-management',
                loadChildren: () => import('src/modules/books-management/books-management.module')
                    .then(m => m.BooksManagementModule)
            },
            {
                path: 'dragon-slayer',
                loadChildren: () => import('src/modules/dragon-slayer/dragon-slayer.module')
                    .then(m => m.DragonSlayerModule)
            },
            {
                path: 'messenger',
                loadChildren: () => import('src/modules/messenger/messenger.module')
                .then(m => m.MessengerModule)
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouting { }
