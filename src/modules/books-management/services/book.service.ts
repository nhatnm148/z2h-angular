import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Book } from 'src/modules/books-management/models/book.model';
import { CreateBookDto } from 'src/modules/books-management/dtos/create-book.dto';
import { UpdateBookDto } from 'src/modules/books-management/dtos/update-book.dto';

@Injectable()
export class BookService {
    baseUrl = environment.baseUrl;

    constructor(
        private http: HttpClient
    ) { }

    getBooks(): Observable<Book[]> {
        console.log(`${this.baseUrl}/books`);
        return this.http.get<Book[]>(`${this.baseUrl}/books`);
    }

    createBook(createBookDto: CreateBookDto): Observable<Book> {
        return this.http.post<Book>(`${this.baseUrl}/books`, createBookDto);
    }

    updateBook(id: string, updateBookDto: UpdateBookDto): Observable<Book> {
        return this.http.put<Book>(`${this.baseUrl}/books/${id}`, updateBookDto);
    }

    deleteBook(id: string): Observable<HttpResponse<void>> {
        return this.http.delete<HttpResponse<void>>(`${this.baseUrl}/books/${id}`);
    }
}
