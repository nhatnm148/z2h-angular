import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-book-delete-pop-up',
  templateUrl: './book-delete-pop-up.component.html',
  styleUrls: ['./book-delete-pop-up.component.css']
})
export class BookDeletePopUpComponent implements OnInit {
  @Output() submission$ = new EventEmitter();
  @Output() cancellation$ = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  submit(): void {
    this.submission$.emit();
  }

  hide(): void {
    this.cancellation$.emit();
  }
}
