import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookDeletePopUpComponent } from './book-delete-pop-up.component';

describe('BookDeletePopUpComponent', () => {
  let component: BookDeletePopUpComponent;
  let fixture: ComponentFixture<BookDeletePopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookDeletePopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDeletePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
