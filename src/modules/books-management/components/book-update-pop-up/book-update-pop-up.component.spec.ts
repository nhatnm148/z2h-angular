import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookUpdatePopUpComponent } from 'src/modules/books-management/components/book-update-pop-up/book-update-pop-up.component';

describe('BookCreatePopUpComponent', () => {
  let component: BookUpdatePopUpComponent;
  let fixture: ComponentFixture<BookUpdatePopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookUpdatePopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookUpdatePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
