import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Book } from 'src/modules/books-management/models/book.model';

@Component({
  selector: 'app-book-update-pop-up',
  templateUrl: './book-update-pop-up.component.html',
  styleUrls: ['./book-update-pop-up.component.css']
})
export class BookUpdatePopUpComponent implements OnInit {
  @Input() selectedBook: Book;
  @Output() submission$ = new EventEmitter();
  @Output() cancellation$ = new EventEmitter();

  updateBookForm = this.fb.group({
    id: '',
    title: ['', Validators.required],
    author: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initFormValue();
  }

  initFormValue(): void {
    this.updateBookForm.patchValue({
      id: this.selectedBook?.id || '',
      title: this.selectedBook?.title || '',
      author: this.selectedBook?.author || ''
    });
  }

  submit(): void {
    this.submission$.emit(this.updateBookForm.value);
  }

  hide(): void {
    this.cancellation$.emit();
  }
}
