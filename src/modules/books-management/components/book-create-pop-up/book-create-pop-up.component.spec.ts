import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCreatePopUpComponent } from './book-create-pop-up.component';

describe('BookCreatePopUpComponent', () => {
  let component: BookCreatePopUpComponent;
  let fixture: ComponentFixture<BookCreatePopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookCreatePopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCreatePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
