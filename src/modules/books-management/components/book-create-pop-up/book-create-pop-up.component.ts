import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-book-create-pop-up',
  templateUrl: './book-create-pop-up.component.html',
  styleUrls: ['./book-create-pop-up.component.css']
})
export class BookCreatePopUpComponent implements OnInit {
  @Output() submission$ = new EventEmitter();
  @Output() cancellation$ = new EventEmitter();

  createBookForm = this.fb.group({
    title: ['', Validators.required],
    author: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  submit(): void {
    this.submission$.emit(this.createBookForm.value);
  }

  hide(): void {
    this.cancellation$.emit();
  }
}
