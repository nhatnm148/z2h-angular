export class UpdateBookDto {
    id: string;
    title: string;
    author: string;
}
