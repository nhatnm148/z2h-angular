import { NgModule } from '@angular/core';
import { BooksManagementRouting } from 'src/modules/books-management/books-management.routing';
import { BooksManagementComponent } from 'src/modules/books-management/pages/books-management.component';
import { TableModule } from 'primeng/table';
import { BooksManagementBiz } from 'src/modules/books-management/biz-models/books-management.biz';
import { BookService } from 'src/modules/books-management/services/book.service';
import { BooksManagementSelectors } from 'src/modules/books-management/state/books-management.selectors';
import { NgxsModule } from '@ngxs/store';
import { BooksManagementState } from 'src/modules/books-management/state/books-management.state';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BookCreatePopUpComponent } from 'src/modules/books-management/components/book-create-pop-up/book-create-pop-up.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookUpdatePopUpComponent } from 'src/modules/books-management/components/book-update-pop-up/book-update-pop-up.component';
import { BookDeletePopUpComponent } from './components/book-delete-pop-up/book-delete-pop-up.component';

@NgModule({
    declarations: [
        BooksManagementComponent,
        BookCreatePopUpComponent,
        BookUpdatePopUpComponent,
        BookDeletePopUpComponent
    ],
    imports: [
        BooksManagementRouting,
        TableModule,
        NgxsModule.forFeature([
            BooksManagementState
        ]),
        ModalModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        BooksManagementBiz,
        BookService,
        BooksManagementSelectors
    ],
    entryComponents: [
        BookCreatePopUpComponent,
        BookUpdatePopUpComponent,
        BookDeletePopUpComponent
    ]
})
export class BooksManagementModule { }
