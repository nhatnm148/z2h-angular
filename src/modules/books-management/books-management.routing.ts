import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BooksManagementComponent } from 'src/modules/books-management/pages/books-management.component';

export const routes: Routes = [
    {
        path: '',
        component: BooksManagementComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class BooksManagementRouting { }
