import { Book } from 'src/modules/books-management/models/book.model';
import { State, StateContext, Action } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { StoreBooksAction, CreateBookAction, UpdateBookAction, DeleteBookAction } from 'src/modules/books-management/state/books-management.actions';

export interface BooksManagementStateModel {
    books: Book[];
}

export const initialState: BooksManagementStateModel = {
    books: []
};

@State({
    name: 'books_management_state',
    defaults: initialState
})
@Injectable()
export class BooksManagementState {
    @Action(StoreBooksAction)
    storeBooksAction(ctx: StateContext<BooksManagementStateModel>, action: StoreBooksAction): void {
        ctx.patchState({
            books: [ ...action.payload ]
        });
    }

    @Action(CreateBookAction)
    createBookAction(ctx: StateContext<BooksManagementStateModel>, action: CreateBookAction): void {
        ctx.patchState({
            books: [ ...ctx.getState().books, action.payload ]
        });
    }

    @Action(UpdateBookAction)
    updateBookAction(ctx: StateContext<BooksManagementStateModel>, action: UpdateBookAction): void {
        const list = [ ...ctx.getState().books];
        const index = list.findIndex(book => book.id === action.id);
        list[index] = { ...action.payload };

        ctx.patchState({
            books: [ ...list ]
        });
    }

    @Action(DeleteBookAction)
    deleteBookAction(ctx: StateContext<BooksManagementStateModel>, action: DeleteBookAction): void {
        const list = [ ...ctx.getState().books];
        const filteredList = [ ...list.filter(book => book.id !== action.id)];

        ctx.patchState({
            books: [ ...filteredList ]
        });
    }
}
