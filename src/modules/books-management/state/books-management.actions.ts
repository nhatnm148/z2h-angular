import { Book } from 'src/modules/books-management/models/book.model';

export class StoreBooksAction {
    static readonly type = '[BOOKS_MANAGEMENT] Store books';
    constructor(public payload: Book[]) { }
}

export class CreateBookAction {
    static readonly type = '[BOOKS_MANAGEMENT] Create book';
    constructor(public payload: Book) { }
}

export class UpdateBookAction {
    static readonly type = '[BOOKS_MANAGEMENT] Update book';
    constructor(public id: string, public payload: Book) { }
}

export class DeleteBookAction {
    static readonly type = '[BOOKS_MANAGEMENT] Delete book';
    constructor(public id: string) { }
}
