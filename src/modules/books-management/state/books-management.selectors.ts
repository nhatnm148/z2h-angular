import { Injectable } from '@angular/core';
import { Selector, StateToken } from '@ngxs/store';
import { BooksManagementState, BooksManagementStateModel } from 'src/modules/books-management/state/books-management.state';
import { Book } from 'src/modules/books-management/models/book.model';

@Injectable()
export class BooksManagementSelectors {
    @Selector([BooksManagementState])
    static books$(state: BooksManagementStateModel): Book[] {
        return state.books;
    }
}
