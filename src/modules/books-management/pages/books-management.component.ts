import { Component, OnInit } from '@angular/core';
import { Book } from 'src/modules/books-management/models/book.model';
import { BooksManagementBiz } from 'src/modules/books-management/biz-models/books-management.biz';
import { Subscription } from 'rxjs';
import {
  CreateRequested,
  UpdateRequested,
  DeleteRequested
} from 'src/modules/books-management/biz-models/books-management.events';

@Component({
  selector: 'app-books-management',
  templateUrl: './books-management.component.html',
  styleUrls: ['./books-management.component.css']
})
export class BooksManagementComponent implements OnInit {
  books: Book[] = [];
  safeSubscription: Subscription[] = [];

  constructor(
    private booksManagementBiz: BooksManagementBiz
  ) { }

  ngOnInit(): void {
    this.booksManagementBiz.initScripts();
    this.registerToBooks();
  }

  registerToBooks(): void {
    this.safeSubscription.push(
      this.booksManagementBiz.books$
        .subscribe(res => {
          this.books = res;
        })
    );
  }

  createBook(): void {
    this.booksManagementBiz.transition(new CreateRequested());
  }

  updateBook(selectedBook: Book): void {
    this.booksManagementBiz.transition(new UpdateRequested(selectedBook));
  }

  deleteBook(id: string): void {
    this.booksManagementBiz.transition(new DeleteRequested(id));
  }
}
