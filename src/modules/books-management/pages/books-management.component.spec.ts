import { async, ComponentFixture, TestBed, fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';

import { BooksManagementComponent } from './books-management.component';
import { BookCreatePopUpComponent } from 'src/modules/books-management/components/book-create-pop-up/book-create-pop-up.component';
import { TableModule } from 'primeng/table';
import { NgxsModule } from '@ngxs/store';
import { BooksManagementState } from 'src/modules/books-management/state/books-management.state';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule, FormGroup, AbstractControl } from '@angular/forms';
import { BooksManagementBiz } from 'src/modules/books-management/biz-models/books-management.biz';
import { BookService } from 'src/modules/books-management/services/book.service';
import { BooksManagementSelectors } from 'src/modules/books-management/state/books-management.selectors';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { first, filter } from 'rxjs/operators';
import { Book } from 'src/modules/books-management/models/book.model';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { CreateRequested, CreateConfirmed } from 'src/modules/books-management/biz-models/books-management.events';

fdescribe('BooksManagementComponent', () => {
  let listFixture: ComponentFixture<BooksManagementComponent>;
  let listComponent: BooksManagementComponent;
  let createPopUpFixture: ComponentFixture<BookCreatePopUpComponent>;
  let createPopUpComponent: BookCreatePopUpComponent;
  let service: BookService;
  let biz: BooksManagementBiz;
  const fetchedBookRes: Book[] = [
    { id: 'BOOK001', title: '300 bài code thiếu nhi', author: 'Hello World' }
  ];
  const bookToCreate: Book = {
    id: 'BOOK002', title: '30 bài code thiếu niên', author: 'Hello universe'
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        BooksManagementComponent
      ],
      imports: [
        HttpClientModule,
        NgxsModule.forRoot([
          BooksManagementState
        ]),
        ModalModule.forRoot(),
        ToastrModule.forRoot(),
        TableModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        BooksManagementBiz,
        BookService,
        BooksManagementSelectors
      ]
    }).compileComponents();

    createPopUpFixture = TestBed.createComponent(BookCreatePopUpComponent);
    createPopUpComponent = createPopUpFixture.componentInstance;
    listFixture = TestBed.createComponent(BooksManagementComponent);
    listComponent = listFixture.componentInstance;
    service = TestBed.inject(BookService);
    biz = TestBed.inject(BooksManagementBiz);

    spyOn(service, 'getBooks').and.returnValue(of(fetchedBookRes));
    spyOn(service, 'createBook').and.returnValue(of(bookToCreate));
    listFixture.detectChanges();
  });

  it('Components should be created', () => {
    expect(listComponent).toBeTruthy();
    expect(createPopUpComponent).toBeTruthy();
  });

  it('Check fetched books with books stored in state', async () => {
    biz.books$
      .pipe(
        filter(books => !!books.length),
        first()
      ).subscribe(books => {
        listFixture.detectChanges();

        expect(books).toEqual(fetchedBookRes);
      });
  });

  describe('CREATE POPUP', () => {
    let createForm: FormGroup;
    let titleControl: AbstractControl;
    let authorControl: AbstractControl;
    let titleField;
    let authorField;
    let createBookEvent: jasmine.Spy;

    beforeEach(() => {
      createBookEvent = spyOn(listComponent, 'createBook').and.callFake(() => {
        biz.transition(new CreateRequested());
      });

      const createBtn = listFixture.debugElement.query(By.css('#createBtn'));
      createBtn.triggerEventHandler('click', {});

      listFixture.detectChanges();

      createForm = createPopUpComponent.createBookForm;
      titleControl = createForm.get('title');
      authorControl = createForm.get('author');
      titleField = createPopUpFixture.debugElement.nativeElement.querySelector('#title');
      authorField = createPopUpFixture.debugElement.nativeElement.querySelector('#author');
    });

    it('Happy case', () => {
      // Check if modal is opened
      expect(createBookEvent).toHaveBeenCalled();

      // Check form's state when initialized
      expect(createForm).toBeTruthy();
      expect(titleControl).toBeTruthy();
      expect(authorControl).toBeTruthy();
      expect(titleField).toBeTruthy();
      expect(authorField).toBeTruthy();

      // Check VALIDITY whtn initialized
      expect(createForm.valid).toBeFalsy();
      expect(titleControl.valid).toBeFalsy();
      expect(authorControl.valid).toBeFalsy();

      // Fill in field by field and check for VALIDITY
      createForm.controls.title.setValue('30 bài code thiếu niên');
      createForm.controls.author.setValue('Hello universe');

      createPopUpFixture.detectChanges();

      expect(createForm.valid).toBeTruthy('Create form should be valid now!');

      const createSubmitEvent = spyOn(createPopUpComponent, 'submit').and.callFake(() => {
        biz.transition(new CreateConfirmed(null));
      });
      const createFormEle = createPopUpFixture.debugElement.query(By.css('#createBtn'));
      createFormEle.nativeElement.click();

      listFixture.detectChanges();

      expect(createSubmitEvent).toHaveBeenCalled();
      biz.books$.pipe(
        filter(books => !!books.length),
        first()
      ).subscribe(books => {
        listFixture.detectChanges();

        fetchedBookRes.push(bookToCreate);

        expect(books).toEqual(fetchedBookRes, 'Books store in state should have new book now!');
      });
    });
  });
});
