import { MachineConfig } from 'xstate';
import {
    BooksManagementEvents,
    FetchSuceed,
    FetchFailed,
    CreateRequested,
    UpdateRequested,
    DeleteRequested,
    CreateConfirmed,
    CreateCanceled,
    CreateSucceed,
    CreateFailed,
    UpdateConfirmed,
    UpdateCanceled,
    UpdateSucceed,
    UpdateFailed,
    DeleteConfirmed,
    DeleteCanceled,
    DeleteSucceed,
    DeleteFailed
} from './books-management.events';
import { BooksManagementStates } from 'src/modules/books-management/biz-models/books-magement-states.enum';

export interface IBooksManagementSchema {
    states: {
        idle: {},
        fetching: {},
        showing: {},
        create: {},
        create_processing: {},
        update: {},
        update_processing: {},
        delete: {},
        delete_processing: {},
        errors: {}
    };
}

export const booksManagementConfig: MachineConfig<
    any,
    IBooksManagementSchema,
    BooksManagementEvents
> = {
    id: 'books_management_state_machine',
    initial: BooksManagementStates.IDLE,
    states: {
        idle: {
            always: BooksManagementStates.FETCHING
        },
        fetching: {
            invoke: {
                id: 'fetchBooks',
                src: 'fetchBooks'
            },
            on: {
                [FetchSuceed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: ['showMessages']
                },
                [FetchFailed.type]: BooksManagementStates.ERRORS
            }
        },
        showing: {
            on: {
                [CreateRequested.type]: BooksManagementStates.CREATE,
                [UpdateRequested.type]: BooksManagementStates.UPDATE,
                [DeleteRequested.type]: BooksManagementStates.DELETE
            }
        },
        create: {
            entry: 'showCreateModal',
            on: {
                [CreateConfirmed.type]: BooksManagementStates.CREATE_PROCESSING,
                [CreateCanceled.type]: BooksManagementStates.SHOWING
            }
        },
        create_processing: {
            invoke: {
                id: 'createBook',
                src: 'createBook'
            },
            on: {
                [CreateSucceed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: ['showMessages']
                },
                [CreateFailed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: 'showMessages'
                }
            }
        },
        update: {
            entry: 'showUpdateModal',
            on: {
                [UpdateConfirmed.type]: BooksManagementStates.UPDATE_PROCESSING,
                [UpdateCanceled.type]: BooksManagementStates.SHOWING
            }
        },
        update_processing: {
            invoke: {
                id: 'updateBook',
                src: 'updateBook'
            },
            on: {
                [UpdateSucceed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: ['showMessages']
                },
                [UpdateFailed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: 'showMessages'
                }
            }
        },
        delete: {
            entry: 'showDeleteModal',
            on: {
                [DeleteConfirmed.type]: BooksManagementStates.DELETE_PROCESSING,
                [DeleteCanceled.type]: BooksManagementStates.SHOWING
            }
        },
        delete_processing: {
            invoke: {
                id: 'deleteBook',
                src: 'deleteBook'
            },
            on: {
                [DeleteSucceed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: ['showMessages']
                },
                [DeleteFailed.type]: {
                    target: BooksManagementStates.SHOWING,
                    actions: 'showMessages'
                }
            }
        },
        errors: {
            type: 'final'
        }
    }
};
