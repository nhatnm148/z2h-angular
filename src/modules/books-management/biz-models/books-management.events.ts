import { EventObject } from 'xstate';
import { Book } from 'src/modules/books-management/models/book.model';
import { CreateBookDto } from 'src/modules/books-management/dtos/create-book.dto';
import { UpdateBookDto } from 'src/modules/books-management/dtos/update-book.dto';

export class FetchSuceed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Fetch Succeed';
    readonly type = FetchSuceed.type;

    constructor(
        public books: Book[],
        public messages: string[]
    ) { }
}

export class FetchFailed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Fetch Failed';
    readonly type = FetchFailed.type;

    constructor(public messages: string[]) { }
}

export class CreateRequested implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Create Requested';
    readonly type = CreateRequested.type;

    constructor() { }
}

export class CreateConfirmed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Create Confirmed';
    readonly type = CreateConfirmed.type;

    constructor(public createBookDto: CreateBookDto) { }
}

export class CreateCanceled implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Create Canceled';
    readonly type = CreateCanceled.type;

    constructor() { }
}

export class CreateSucceed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Create Succeed';
    readonly type = CreateSucceed.type;

    constructor(public messages: string[]) { }
}

export class CreateFailed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Create Failed';
    readonly type = CreateFailed.type;

    constructor(public messages: string[]) { }
}

export class UpdateRequested implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Update Requested';
    readonly type = UpdateRequested.type;

    constructor(public selectedBook: Book) { }
}

export class UpdateConfirmed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Update Confirmed';
    readonly type = UpdateConfirmed.type;

    constructor(
        public id: string,
        public updateBookDto: UpdateBookDto
    ) { }
}

export class UpdateCanceled implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Update Canceled';
    readonly type = UpdateCanceled.type;

    constructor() { }
}

export class UpdateSucceed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Update Succeed';
    readonly type = UpdateSucceed.type;

    constructor(public messages: string[]) { }
}

export class UpdateFailed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Update Failed';
    readonly type = UpdateFailed.type;

    constructor(public messages: string[]) { }
}

export class DeleteRequested implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Delete Requested';
    readonly type = DeleteRequested.type;

    constructor(public id: string) { }
}

export class DeleteConfirmed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Delete Confirmed';
    readonly type = DeleteConfirmed.type;

    constructor(public id: string) { }
}

export class DeleteCanceled implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Delete Canceled';
    readonly type = DeleteCanceled.type;

    constructor() { }
}

export class DeleteSucceed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Delete Succeed';
    readonly type = DeleteSucceed.type;

    constructor(public messages: string[]) { }
}

export class DeleteFailed implements EventObject {
    static readonly type = '[BOOKS_MANAGEMENT] Delete Failed';
    readonly type = DeleteFailed.type;

    constructor(public messages: string[]) { }
}

export type BooksManagementEvents =
    FetchSuceed | FetchFailed |
    CreateRequested | CreateConfirmed | CreateCanceled | CreateSucceed | CreateFailed |
    UpdateRequested | UpdateConfirmed | UpdateCanceled | UpdateSucceed | UpdateFailed |
    DeleteRequested | DeleteConfirmed | DeleteCanceled | DeleteSucceed | DeleteFailed;
