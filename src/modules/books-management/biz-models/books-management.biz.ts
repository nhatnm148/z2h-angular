import { Injectable } from '@angular/core';
import { BookService } from 'src/modules/books-management/services/book.service';
import { Store, Select } from '@ngxs/store';
import { StoreBooksAction, CreateBookAction, UpdateBookAction, DeleteBookAction } from 'src/modules/books-management/state/books-management.actions';
import { BooksManagementSelectors } from 'src/modules/books-management/state/books-management.selectors';
import { Observable, of, from } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';
import { Book } from 'src/modules/books-management/models/book.model';
import { MachineOptions, AnyEventObject, Machine, interpret, EventObject } from 'xstate';
import {
    BooksManagementEvents,
    FetchSuceed,
    FetchFailed,
    CreateConfirmed,
    CreateSucceed,
    CreateFailed,
    UpdateConfirmed,
    UpdateSucceed,
    UpdateFailed,
    DeleteConfirmed,
    DeleteSucceed,
    DeleteFailed,
    CreateCanceled,
    UpdateRequested,
    UpdateCanceled,
    DeleteRequested,
    DeleteCanceled
} from 'src/modules/books-management/biz-models/books-management.events';
import { IBooksManagementSchema, booksManagementConfig } from 'src/modules/books-management/biz-models/books-management.config';
import { BookCreatePopUpComponent } from 'src/modules/books-management/components/book-create-pop-up/book-create-pop-up.component';
import { CreateBookDto } from 'src/modules/books-management/dtos/create-book.dto';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BookUpdatePopUpComponent } from 'src/modules/books-management/components/book-update-pop-up/book-update-pop-up.component';
import { UpdateBookDto } from 'src/modules/books-management/dtos/update-book.dto';
import { BookDeletePopUpComponent } from 'src/modules/books-management/components/book-delete-pop-up/book-delete-pop-up.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class BooksManagementBiz {
    @Select(BooksManagementSelectors.books$)
    readonly books$: Observable<Book[]>;
    private modalRef;
    private service;
    private machine;
    private booksManagementOptions: Partial<
        MachineOptions<
            any,
            BooksManagementEvents
        >
    > = {
            actions: {
                showMessages: (_, event: AnyEventObject) => {
                    event.messages.forEach(msg => console.log(msg));
                },
                showCreateModal: () => {
                    this.showCreateModalHandler();
                },
                showUpdateModal: (_, event: UpdateRequested) => {
                    this.showUpdateModalHandler(event);
                },
                showDeleteModal: (_, event: DeleteRequested) => {
                    this.showDeleteModalHandler(event);
                }
            },
            services: {
                fetchBooks: () => {
                    return this.fetchBooksHandler();
                },
                createBook: (_, event: CreateConfirmed) => {
                    return this.createBookHandler(event);
                },
                updateBook: (_, event: UpdateConfirmed) => {
                    return this.updateBookHandler(event);
                },
                deleteBook: (_, event: DeleteConfirmed) => {
                    return this.deleteBookHandler(event);
                }
            }
        };

    constructor(
        private bookService: BookService,
        private store: Store,
        private modalService: BsModalService,
        private spinnerService: NgxSpinnerService,
        private toastrService: ToastrService
    ) { }

    initScripts(): void {
        this.machine = Machine<
            any,
            IBooksManagementSchema,
            BooksManagementEvents
        >(booksManagementConfig).withConfig(this.booksManagementOptions);

        this.service = interpret(this.machine).start();

        const state$ = from(this.service);
        state$.subscribe((res: any) => {
            console.log(`-----------------------------------------`);
            console.log(`Event sent: ${res.transitions[0]?.event || ''}`);
            console.log(`Current state: ${res.value}`);
            console.log(`-----------------------------------------`);
        });
    }

    transition(event: EventObject): void {
        this.service.send(event);
    }

    // Actions handlers
    showCreateModalHandler(): void {
        this.modalRef = this.modalService.show(
            BookCreatePopUpComponent,
            {
                class: 'modal-lg',
                backdrop: true,
                ignoreBackdropClick: true
            }
        );

        this.modalRef.content.submission$
            .subscribe((res: CreateBookDto) => {
                this.transition(new CreateConfirmed(res));
            });

        this.modalRef.content.cancellation$
            .subscribe(() => {
                this.transition(new CreateCanceled());
                this.modalRef.hide();
            });
    }

    showUpdateModalHandler(event: UpdateRequested): void {
        const { selectedBook } = event;

        this.modalRef = this.modalService.show(
            BookUpdatePopUpComponent,
            {
                class: 'modal-lg',
                backdrop: true,
                ignoreBackdropClick: true,
                initialState: { selectedBook }
            }
        );

        this.modalRef.content.submission$
            .subscribe((res: UpdateBookDto) => {
                this.transition(new UpdateConfirmed(res.id, res));
            });

        this.modalRef.content.cancellation$
            .subscribe(() => {
                this.transition(new UpdateCanceled());
                this.modalRef.hide();
            });
    }

    showDeleteModalHandler(event: DeleteRequested): void {
        this.modalRef = this.modalService.show(
            BookDeletePopUpComponent,
            {
                class: 'modal-lg',
                backdrop: true,
                ignoreBackdropClick: true
            }
        );

        this.modalRef.content.submission$
            .subscribe(() => {
                this.transition(new DeleteConfirmed(event.id));
            });

        this.modalRef.content.cancellation$
            .subscribe(() => {
                this.transition(new DeleteCanceled());
                this.modalRef.hide();
            });
    }
    // End actions handlers

    // Services handlers
    fetchBooksHandler(): Observable<BooksManagementEvents> {
        this.spinnerService.show();

        return this.bookService
            .getBooks()
            .pipe(
                map(books => {
                    const msg = 'Fetched successfully!';

                    this.showSuccessMsg(msg);

                    this.store.dispatch(new StoreBooksAction(books));

                    return new FetchSuceed(books, [msg]);
                }),
                catchError(() => {
                    const msg = 'Failed to fetch!';

                    this.showErrMsg(msg);

                    return of(new FetchFailed([msg]));
                }),
                finalize(() => {
                    setTimeout(() => {
                        this.spinnerService.hide();
                    }, 200);
                })
            );
    }

    createBookHandler(event: CreateConfirmed): Observable<BooksManagementEvents> {
        this.spinnerService.show();

        return this.bookService
            .createBook(event.createBookDto)
            .pipe(
                map(createdBook => {
                    const msg = 'Created successfully!';

                    this.showSuccessMsg(msg);

                    this.store.dispatch(new CreateBookAction(createdBook));

                    return new CreateSucceed([msg]);
                }),
                catchError(() => {
                    const msg = 'Failed to create!';

                    this.showErrMsg(msg);

                    return of(new CreateFailed([msg]));
                }),
                finalize(() => {
                    setTimeout(() => {
                        this.spinnerService.hide();
                    }, 200);
                    this.modalRef.hide();
                })
            );
    }

    updateBookHandler(event: UpdateConfirmed): Observable<BooksManagementEvents> {
        this.spinnerService.show();

        return this.bookService
            .updateBook(event.id, event.updateBookDto)
            .pipe(
                map(updatedBook => {
                    const msg = 'Updated successfully!';

                    this.showSuccessMsg(msg);

                    this.store.dispatch(
                        new UpdateBookAction(updatedBook.id, updatedBook)
                    );

                    return new UpdateSucceed([msg]);
                }),
                catchError(() => {
                    const msg = 'Failed to update!';

                    this.showErrMsg(msg);

                    return of(new UpdateFailed([msg]));
                }),
                finalize(() => {
                    setTimeout(() => {
                        this.spinnerService.hide();
                    }, 200);
                    this.modalRef.hide();
                })
            );
    }

    deleteBookHandler(event: DeleteConfirmed): Observable<BooksManagementEvents> {
        this.spinnerService.show();

        return this.bookService
            .deleteBook(event.id)
            .pipe(
                map(() => {
                    const msg = 'Delete successfully!';

                    this.showSuccessMsg(msg);

                    this.store.dispatch(
                        new DeleteBookAction(event.id)
                    );

                    return new DeleteSucceed([msg]);
                }),
                catchError(() => {
                    const msg = 'Failed to delete';

                    this.showErrMsg(msg);

                    return of(new DeleteFailed([msg]));
                }),
                finalize(() => {
                    setTimeout(() => {
                        this.spinnerService.hide();
                    }, 200);
                    this.modalRef.hide();
                })
            );
    }
    // End events handlers

    showSuccessMsg(msg: string): void {
        this.toastrService.clear();
        this.toastrService.success(msg);
    }

    showErrMsg(msg: string): void {
        this.toastrService.clear();
        this.toastrService.error(msg);
    }
}
