export enum BooksManagementStates {
    IDLE = 'idle',
    FETCHING = 'fetching',
    SHOWING = 'showing',
    CREATE = 'create',
    CREATE_PROCESSING = 'create_processing',
    UPDATE = 'update',
    UPDATE_PROCESSING = 'update_processing',
    DELETE = 'delete',
    DELETE_PROCESSING = 'delete_processing',
    ERRORS = 'errors'
}
