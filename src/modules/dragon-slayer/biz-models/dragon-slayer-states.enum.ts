export enum DragonSlayerStates {
    IDLE = 'idle',
    HEROES_LIST_FETCHING = 'heroes_list_fetching',
    HEROES_LIST_SHOWING = 'heroes_list_showing',
    ESSENTIAL_DATA_FETCHING = 'essential_data_fetching',
    ERRORS = 'errors'
}
