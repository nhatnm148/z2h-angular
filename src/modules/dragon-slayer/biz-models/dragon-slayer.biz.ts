import { Injectable } from '@angular/core';
import { HeroService } from 'src/modules/dragon-slayer/services/hero.service';
import { MachineOptions, AnyEventObject, Machine, interpret, StateMachine, Interpreter } from 'xstate';
import { IDragonSlayerContext, IDragonSlayerSchema, dragonSlayerConfig } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.config';
import { DragonSlayerEvents, FetchHeroesListRequested, FetchHeroesListSucceed, FetchHeroesListFailed } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.events';
import { from, of, Observable, zip } from 'rxjs';
import { map, catchError, first } from 'rxjs/operators';
import { Store, Select } from '@ngxs/store';
import { StoreHeroesAction, StoreSelectedHeroAction, StoreDragonsAction } from 'src/modules/dragon-slayer/state/dragon-slayer.actions';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';
import { DragonSlayerSelectors } from 'src/modules/dragon-slayer/state/dragon-slayer.selectors';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';
import { DragonService } from 'src/modules/dragon-slayer/services/dragon.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { v4 } from 'uuid';
import { Socket } from 'ngx-socket-io';
import { DragonSlayerService } from 'src/modules/dragon-slayer/services/dragon-slayer.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable()
export class DragonSlayerBiz {
    @Select(DragonSlayerSelectors.heroes$) readonly heroes$: Observable<Hero[]>;
    @Select(DragonSlayerSelectors.selectedHero$) readonly selectedHero$: Observable<Hero>;
    @Select(DragonSlayerSelectors.dragons$) readonly dragons$: Observable<Dragon[]>;

    // private machine;
    // private service;
    // private dragonSlayerOptions: Partial<
    //     MachineOptions<IDragonSlayerContext, DragonSlayerEvents>
    // > = {
    //         actions: {
    //             showMessage: (_, event: AnyEventObject) => {
    //                 console.log(event.msg);
    //             },
    //             storeHeroes: (_, event: FetchHeroesListSucceed) => {
    //                 this.store.dispatch(new StoreHeroesAction(event.heroes));
    //             }
    //         },
    //         services: {
    //             fetchHeroes: () => {
    //                 return this.fetchHeroesHandler();
    //             }
    //         }
    //     };

    constructor(
        private heroService: HeroService,
        private dragonService: DragonService,
        private dragonSlayerService: DragonSlayerService,
        private store: Store,
        private spinnerService: NgxSpinnerService,
        private socket: Socket,
        private toastrService: ToastrService,
        private router: Router
    ) {
        // this.initScript();
    }

    // initScript(): void {
    //     this.machine = Machine<
    //         IDragonSlayerContext, IDragonSlayerSchema, DragonSlayerEvents
    //     >(dragonSlayerConfig).withConfig(this.dragonSlayerOptions);

    //     this.service = interpret(this.machine).start();

    //     const state$ = from(this.service);
    //     state$.subscribe((state: any) => {
    //         console.log(`-----------------------------------------`);
    //         console.log(`Event sent: ${state.transitions[0]?.event || ''}`);
    //         console.log(`Current state: ${state.value}`);
    //         console.log(`-----------------------------------------`);
    //     });
    // }

    // transition(event: DragonSlayerEvents): void {
    //     this.service.send(event);
    // }

    // fetchHeroesHandler(): Observable<AnyEventObject> {
    //     return this.heroService
    //         .fetch()
    //         .pipe(
    //             map(heroes => {
    //                 return new FetchHeroesListSucceed(heroes);
    //             }),
    //             catchError(() => {
    //                 return of(new FetchHeroesListFailed());
    //             })
    //         );
    // }

    fetchHeroes(): void {
        this.heroService.fetch().subscribe(heroes => {
            this.store.dispatch(new StoreHeroesAction(heroes));
        });
    }

    getHeroById(heroId: string): void {
        this.heroService.getHeroById(heroId).subscribe(hero => {
            this.store.dispatch(new StoreSelectedHeroAction(hero));
        });
    }

    fetchDragons(): void {
        this.dragonService.fetch().subscribe(dragons => {
            this.store.dispatch(new StoreDragonsAction(dragons));
        });
    }

    heroFightsDragon(heroId: string, dragonId: string): void {
        this.spinnerService.show();
        const commandId = v4();

        this.socket.fromEvent(commandId)
            .pipe(first())
            .subscribe((res: any) => {
                zip(
                    this.heroService.getHeroById(heroId),
                    this.dragonService.fetch()
                ).subscribe(
                    ([hero, dragons]) => {
                        this.store.dispatch(new StoreSelectedHeroAction(hero));
                        this.store.dispatch(new StoreDragonsAction(dragons));

                        this.spinnerService.hide();

                        if (res.hero && res.dragon && res.hero.status === 'alive') {
                            this.toastrService
                                .success('You has killed the dragon and got ' + res.goldsGot + ' golds!', null, { timeOut: 5000 });
                        } else if (res.hero && res.dragon && res.hero.status === 'dead') {
                            this.toastrService
                                .warning('You have been killed!! Goodbye hero!', null, { timeOut: 5000 });
                            this.router.navigateByUrl('/z2h/dragon-slayer');
                        } else {
                            this.toastrService
                                .error('Errors has occurred when you tried to attack the dragon!!', null, { timeOut: 5000 });
                        }
                    }
                );
            });

        this.dragonSlayerService
            .heroFightsDragon(heroId, dragonId, commandId)
            .subscribe();
    }
}
