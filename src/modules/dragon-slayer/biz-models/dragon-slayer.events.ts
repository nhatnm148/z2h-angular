import { EventObject } from 'xstate';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';

export class FetchHeroesListRequested implements EventObject {
    static readonly type = '[DRAGON SLAYER] Fetch Heroes List Requested';
    readonly type = FetchHeroesListRequested.type;

    constructor() { }
}

export class FetchHeroesListSucceed implements EventObject {
    static readonly type = '[DRAGON SLAYER] Fetch Heroes List Succeed';
    readonly type = FetchHeroesListSucceed.type;

    constructor(
        public heroes: Hero[],
        public msg: string = 'Fetched successfully!'
    ) { }
}

export class FetchHeroesListFailed implements EventObject {
    static readonly type = '[DRAGON SLAYER] Fetch Heroes List Failed';
    readonly type = FetchHeroesListFailed.type;

    constructor(public msg: string = 'Failed to fetch!') { }
}

export type DragonSlayerEvents = FetchHeroesListRequested | FetchHeroesListSucceed | FetchHeroesListFailed;
