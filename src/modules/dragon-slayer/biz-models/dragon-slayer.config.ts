import { MachineConfig } from 'xstate';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';
import { DragonSlayerStates } from 'src/modules/dragon-slayer/biz-models/dragon-slayer-states.enum';
import { DragonSlayerEvents, FetchHeroesListRequested, FetchHeroesListSucceed, FetchHeroesListFailed } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.events';

export interface IDragonSlayerContext {
    selected_hero: Hero;
}

export interface IDragonSlayerSchema {
    states: {
        idle: {},
        heroes_list_fetching: {},
        heroes_list_showing: {},
        // essential_data_fetching: {},
        errors: {}
    };
}

const context: IDragonSlayerContext = {
    selected_hero: null
};

export const dragonSlayerConfig: MachineConfig<
    IDragonSlayerContext,
    IDragonSlayerSchema,
    DragonSlayerEvents
> = {
    id: 'dragon_slayer_script',
    context,
    initial: DragonSlayerStates.IDLE,
    states: {
        idle: {
            on: {
                [FetchHeroesListRequested.type]: DragonSlayerStates.HEROES_LIST_FETCHING
            }
        },
        heroes_list_fetching: {
            invoke: {
                id: 'fetchHeroes',
                src: 'fetchHeroes'
            },
            on: {
                [FetchHeroesListSucceed.type]: {
                    target: DragonSlayerStates.HEROES_LIST_SHOWING,
                    actions: ['showMessage', 'storeHeroes']
                },
                [FetchHeroesListFailed.type]: {
                    target: DragonSlayerStates.ERRORS,
                    actions: 'showMessage'
                }
            }
        },
        heroes_list_showing: {
            type: 'final'
        },
        errors: {
            type: 'final'
        }
    }
};
