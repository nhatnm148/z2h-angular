import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragonSlayerComponent } from './dragon-slayer.component';

describe('DragonSlayerComponent', () => {
  let component: DragonSlayerComponent;
  let fixture: ComponentFixture<DragonSlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragonSlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragonSlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
