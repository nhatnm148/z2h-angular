import { Component, OnInit } from '@angular/core';
import { DragonSlayerBiz } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.biz';
import { FetchHeroesListRequested } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.events';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';

@Component({
  selector: 'app-dragon-slayer',
  templateUrl: './dragon-slayer.component.html',
  styleUrls: ['./dragon-slayer.component.css']
})
export class DragonSlayerComponent implements OnInit {
  heroes: Hero[];

  constructor(
    private dragonSlayerBiz: DragonSlayerBiz
  ) { }

  ngOnInit(): void {
    this.fetchHeroes();
  }

  fetchHeroes(): void {
    this.dragonSlayerBiz.heroes$.subscribe(heroes => this.heroes = [ ...heroes ]);
    this.dragonSlayerBiz.fetchHeroes();
  }
}
