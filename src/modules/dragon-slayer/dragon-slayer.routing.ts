import { Routes, RouterModule } from '@angular/router';
import { DragonSlayerComponent } from 'src/modules/dragon-slayer/pages/dragon-slayer.component';
import { DragonsBoardComponent } from 'src/modules/dragon-slayer/components/dragons-board/dragons-board.component';

export const routes: Routes = [
    {
        path: '',
        component: DragonSlayerComponent
    },
    {
        path: 'dragons-board/:id',
        component: DragonsBoardComponent
    }
];

export const DragonSlayerRouting = RouterModule.forChild(routes);
