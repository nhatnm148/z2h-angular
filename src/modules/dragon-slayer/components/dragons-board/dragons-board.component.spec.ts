import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragonsBoardComponent } from './dragons-board.component';

describe('DragonsBoardComponent', () => {
  let component: DragonsBoardComponent;
  let fixture: ComponentFixture<DragonsBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragonsBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragonsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
