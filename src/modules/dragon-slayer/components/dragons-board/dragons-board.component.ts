import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';
import { DragonSlayerBiz } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.biz';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dragons-board',
  templateUrl: './dragons-board.component.html',
  styleUrls: ['./dragons-board.component.css']
})
export class DragonsBoardComponent implements OnInit {
  hero: Hero[] = [];
  dragons: Dragon[] = [];
  safeSubscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private dragonSlayerBiz: DragonSlayerBiz
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.getHeroById(id);
    this.fetchDragons();
  }

  getHeroById(heroId: string): void {
    this.safeSubscriptions.push(
      this.dragonSlayerBiz.selectedHero$.subscribe(hero => {
        if (hero) {
          this.hero = [hero];
        }
      })
    );

    this.dragonSlayerBiz.getHeroById(heroId);
  }

  fetchDragons(): void {
    this.safeSubscriptions.push(
      this.dragonSlayerBiz.dragons$
        .subscribe(dragons => this.dragons = [...dragons])
    );

    this.dragonSlayerBiz.fetchDragons();
  }

  fightDragon(dragonId: string): void {
    this.dragonSlayerBiz.heroFightsDragon(
      this.route.snapshot.paramMap.get('id'),
      dragonId
    );
  }
}
