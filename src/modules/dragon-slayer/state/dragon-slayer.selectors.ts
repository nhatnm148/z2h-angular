import { Injectable } from '@angular/core';
import { Selector } from '@ngxs/store';
import { DragonSlayerState, DragonSlayerStateModel } from 'src/modules/dragon-slayer/state/dragon-slayer.state';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';

@Injectable()
export class DragonSlayerSelectors {
    @Selector([DragonSlayerState])
    static heroes$(state: DragonSlayerStateModel): Hero[] {
        return state.heroes;
    }

    @Selector([DragonSlayerState])
    static selectedHero$(state: DragonSlayerStateModel): Hero {
        return state.selectedHero;
    }

    @Selector([DragonSlayerState])
    static dragons$(state: DragonSlayerStateModel): Dragon[] {
        return state.dragons;
    }
}
