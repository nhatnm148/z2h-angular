import { Hero } from 'src/modules/dragon-slayer/models/hero.model';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';

export class StoreHeroesAction {
    static readonly type = '[DRAGON_SLAYER] Store Heroes';
    constructor(public payload: Hero[]) { }
}

export class StoreDragonsAction {
    static readonly type = '[DRAGON_SLAYER] Store Dragons';
    constructor(public payload: Dragon[]) { }
}

export class StoreSelectedHeroAction {
    static readonly type = '[DRAGON_SLAYER] Store Selected Hero';
    constructor(public payload: Hero) { }
}
