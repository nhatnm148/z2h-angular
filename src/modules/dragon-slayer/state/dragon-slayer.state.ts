import { Hero } from 'src/modules/dragon-slayer/models/hero.model';
import { State, StateContext, Action, actionMatcher } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { StoreHeroesAction, StoreSelectedHeroAction, StoreDragonsAction } from 'src/modules/dragon-slayer/state/dragon-slayer.actions';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';

export interface DragonSlayerStateModel {
    heroes: Hero[];
    selectedHero: Hero;
    dragons: Dragon[];
}

export const initialState: DragonSlayerStateModel = {
    heroes: [],
    selectedHero: null,
    dragons: []
};

@State({
    name: 'dragon_slayer_state',
    defaults: initialState
})
@Injectable()
export class DragonSlayerState {
    @Action(StoreHeroesAction)
    storeHeroesAction(ctx: StateContext<DragonSlayerStateModel>, action: StoreHeroesAction): void {
        ctx.patchState({
            heroes: [...action.payload]
        });
    }

    @Action(StoreDragonsAction)
    storeDragonsAction(ctx: StateContext<DragonSlayerStateModel>, action: StoreDragonsAction): void {
        ctx.patchState({
            dragons: [...action.payload]
        });
    }

    @Action(StoreSelectedHeroAction)
    storeSelectedHeroAction(ctx: StateContext<DragonSlayerStateModel>, action: StoreSelectedHeroAction): void {
        ctx.patchState({
            selectedHero: action.payload
        });
    }
}
