export class Dragon {
    id: string;
    stats: Stats;
    name: string;
    status: DragonStatuses;
}

export class Stats {
    hp: number;
    currentHp: number;
    strength: number;
}

export enum DragonStatuses {
    ALIVE = 'alive',
    DEAD = 'dead'
}
