export class Hero {
    id: string;
    stats: Stats;
    name: string;
    golds: number;
    status: string;
}

export class Stats {
    hp: number;
    currentHp: number;
    strength: number;
}
