import { NgModule } from '@angular/core';
import { DragonSlayerComponent } from 'src/modules/dragon-slayer/pages/dragon-slayer.component';
import { DragonSlayerRouting } from 'src/modules/dragon-slayer/dragon-slayer.routing';
import { TableModule } from 'primeng/table';
import { NgxsModule } from '@ngxs/store';
import { DragonSlayerState } from 'src/modules/dragon-slayer/state/dragon-slayer.state';
import { HeroService } from 'src/modules/dragon-slayer/services/hero.service';
import { DragonSlayerSelectors } from 'src/modules/dragon-slayer/state/dragon-slayer.selectors';
import { DragonSlayerBiz } from 'src/modules/dragon-slayer/biz-models/dragon-slayer.biz';
import { DragonService } from 'src/modules/dragon-slayer/services/dragon.service';
import { DragonsBoardComponent } from './components/dragons-board/dragons-board.component';
import { DragonSlayerService } from 'src/modules/dragon-slayer/services/dragon-slayer.service';

@NgModule({
    declarations: [
        DragonSlayerComponent,
        DragonsBoardComponent
    ],
    imports: [
        DragonSlayerRouting,
        TableModule,
        NgxsModule.forFeature([
            DragonSlayerState
        ])
    ],
    providers: [
        HeroService,
        DragonService,
        DragonSlayerSelectors,
        DragonSlayerBiz,
        DragonSlayerService
    ]
})
export class DragonSlayerModule { }
