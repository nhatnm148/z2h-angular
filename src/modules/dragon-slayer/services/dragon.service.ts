import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';

@Injectable()
export class DragonService {
    private baseUrl = environment.baseUrl;

    constructor(
        private http: HttpClient
    ) { }

    fetch(): Observable<Dragon[]> {
        return this.http.get<Dragon[]>(`${this.baseUrl}/dragons`);
    }
}
