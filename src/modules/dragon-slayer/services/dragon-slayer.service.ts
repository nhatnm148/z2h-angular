import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Dragon } from 'src/modules/dragon-slayer/models/dragon.model';

@Injectable()
export class DragonSlayerService {
    private baseUrl = environment.baseUrl;

    constructor(
        private http: HttpClient
    ) { }

    heroFightsDragon(heroId: string, dragonId: string, commandId: string): Observable<any> {
        return this.http.post<Dragon[]>(
            `${this.baseUrl}/dragon-slayer-process-manager/hero-fights-dragon-command/${heroId}/${dragonId}/${commandId}`,
            {}
        );
    }
}
