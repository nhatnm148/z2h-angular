import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Hero } from 'src/modules/dragon-slayer/models/hero.model';

@Injectable()
export class HeroService {
    private baseUrl = environment.baseUrl;

    constructor(
        private http: HttpClient
    ) { }

    fetch(): Observable<Hero[]> {
        return this.http.get<Hero[]>(`${this.baseUrl}/heroes`);
    }

    getHeroById(heroId: string): Observable<Hero> {
        return this.http.get<Hero>(`${this.baseUrl}/heroes/${heroId}`);
    }
}
