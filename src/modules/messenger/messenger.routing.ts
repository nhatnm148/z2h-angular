import { RouterModule } from '@angular/router';
import { MessengerComponent } from 'src/modules/messenger/pages/messenger.component';

export const MessengerRouting = RouterModule.forChild([
    {
        path: '',
        component: MessengerComponent
    }
]);
