import { NgModule } from '@angular/core';
import { MessengerComponent } from 'src/modules/messenger/pages/messenger.component';
import { MessengerRouting } from 'src/modules/messenger/messenger.routing';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MessengerInterfaceComponent } from './components/messenger-interface/messenger-interface.component';
import { LoginInterfaceComponent } from './components/login-interface/login-interface.component';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MessengerUserService } from 'src/modules/messenger/services/messenger-user.service';
import { MessengerService } from 'src/modules/messenger/services/messenger.service';
import { NgxsModule } from '@ngxs/store';
import { MessengerState } from 'src/modules/messenger/state/messenger.state';
import { MessengerSelectors } from 'src/modules/messenger/state/messenger.selectors';
import { MessengerBiz } from 'src/modules/messenger/biz-models/messenger.biz';

@NgModule({
    declarations: [
        MessengerComponent,
        MessengerInterfaceComponent,
        LoginInterfaceComponent
    ],
    imports: [
        MessengerRouting,
        AutoCompleteModule,
        ModalModule.forRoot(),
        ReactiveFormsModule,
        CommonModule,
        FormsModule,
        NgxsModule.forFeature([MessengerState])
    ],
    providers: [
        MessengerUserService,
        MessengerService,
        MessengerSelectors,
        MessengerBiz
    ]
})
export class MessengerModule { }
