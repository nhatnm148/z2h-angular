import { Injectable } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { MessengerSelectors } from 'src/modules/messenger/state/messenger.selectors';
import { Observable } from 'rxjs';
import { User } from 'src/modules/messenger/models/user.model';
import { UserCredentialsDto } from 'src/modules/messenger/dtos/user-credentials.dto';
import { MessengerUserService } from 'src/modules/messenger/services/messenger-user.service';
import { ToastrService } from 'ngx-toastr';
import { StoreUserAction, StoreUserMessageHistoriesAction } from 'src/modules/messenger/state/messenger.actions';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserMessageHistory } from 'src/modules/messenger/models/user-message-history.model';
import { MessengerService } from 'src/modules/messenger/services/messenger.service';

@Injectable()
export class MessengerBiz {
    @Select(MessengerSelectors.user$) readonly user$: Observable<User>;
    @Select(MessengerSelectors.userMessageHistories$)
    readonly userMessageHistories$: Observable<UserMessageHistory[]>;

    constructor(
        private store: Store,
        private messengerUserService: MessengerUserService,
        private messengerService: MessengerService,
        private toastrService: ToastrService
    ) { }

    getUserMessageHistories(userId): void {
        this.messengerService
            .getHistories(userId)
            .subscribe(histories => {
                this.store.dispatch(new StoreUserMessageHistoriesAction(histories));
            });
    }

    login(payload: UserCredentialsDto, modalRef: BsModalRef): void {
        this.messengerUserService
            .login(payload)
            .subscribe(
                res => {
                    this.toastrService.success('Logged-in successfully!');
                    this.store.dispatch(new StoreUserAction(res.body));
                    this.getUserMessageHistories(res.body.id);
                    modalRef.hide();
                },
                err => {
                    this.toastrService.error(err.error.message);
                }
            );
    }
}
