import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-interface',
  templateUrl: './login-interface.component.html',
  styleUrls: ['./login-interface.component.css']
})
export class LoginInterfaceComponent implements OnInit {
  @Output() submission$ = new EventEmitter<boolean>();
  @Output() cancellation$ = new EventEmitter<boolean>();
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  back(): void {
    this.cancellation$.emit(true);
  }

  submit(): void {
    this.submission$.emit(this.loginForm.getRawValue());
  }
}
