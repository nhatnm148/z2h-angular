import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessengerInterfaceComponent } from './messenger-interface.component';

describe('MessengerInterfaceComponent', () => {
  let component: MessengerInterfaceComponent;
  let fixture: ComponentFixture<MessengerInterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessengerInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
