import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BsModalService } from 'ngx-bootstrap/modal';
import { LoginInterfaceComponent } from 'src/modules/messenger/components/login-interface/login-interface.component';
import { Router } from '@angular/router';
import { MessengerBiz } from 'src/modules/messenger/biz-models/messenger.biz';
import { User } from 'src/modules/messenger/models/user.model';
import { Subscription } from 'rxjs';
import { UserMessageHistory, UserMessage } from 'src/modules/messenger/models/user-message-history.model';
import { Message, CreateMessageDto } from 'src/modules/messenger/dtos/create-message.dto';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.css']
})
export class MessengerComponent implements OnInit, AfterViewChecked, OnDestroy {
  @ViewChild('scrollBottom') private myScrollContainer: ElementRef;
  user: User;
  userMessageHistories: UserMessageHistory[] = [];
  selectedHistory: Message[] = [];
  suggestedUsers: any = [];
  searchUserValue = '';
  safeSubscription: Subscription[] = [];
  selectedChat = 0;
  content = '';

  constructor(
    private socket: Socket,
    private modalService: BsModalService,
    private router: Router,
    private messengerBiz: MessengerBiz
  ) { }

  ngOnInit(): void {
    this.registerToUser();
    this.registerToUserMessageHistories();
  }

  ngAfterViewChecked() {
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
  }

  registerToUser(): void {
    this.safeSubscription.push(
      this.messengerBiz.user$.subscribe(user => {
        this.user = user;

        if (!this.user) {
          this.loginFormProcess();
        } else {
          this.socket.fromEvent(this.user.id).subscribe((msg: any) => {
            console.log(msg);
            const idx = this.userMessageHistories.findIndex(history => {
              return [msg.from, msg.to].includes(history.from.id)
                && [msg.from, msg.to].includes(history.to.id);
            });

            this.userMessageHistories[idx].messages.unshift(msg.message);

            if (idx === this.selectedChat) {
              this.selectChat(idx);
            }
          });
        }
      })
    );
  }

  registerToUserMessageHistories(): void {
    this.safeSubscription.push(
      this.messengerBiz.userMessageHistories$.subscribe(histories => {
        this.userMessageHistories = histories;

        if (!!this.userMessageHistories.length) {
          this.selectedHistory = cloneDeep(this.userMessageHistories[this.selectedChat].messages);
          this.selectedHistory.reverse();
        }
      })
    );
  }

  loginFormProcess(): void {
    const modalRef = this.modalService.show(LoginInterfaceComponent, {
      class: 'modal-md',
      backdrop: true,
      ignoreBackdropClick: true
    });

    modalRef.content.submission$.subscribe(user => {
      this.messengerBiz.login(user, modalRef);
    });

    modalRef.content.cancellation$.subscribe(() => {
      modalRef.hide();
      this.router.navigateByUrl('/dashboard');
    });
  }

  searchUsers(event: any) {
    this.suggestedUsers = [{ id: 'BOOK001', name: 'Nhật', username: 'nhatnm' }];
  }

  send(): void {
    const msg: CreateMessageDto = {
      from: this.user.id,
      to: this.userMessageHistories[this.selectedChat].to.id,
      message: {
        content: this.content,
        sender: this.user.id
      }
    };

    this.socket.emit('receive', msg);

    const fullMsg: UserMessage = {
      ...msg.message,
      date: new Date().toISOString()
    };

    this.content = '';
  }

  selectChat(i: number): void {
    this.selectedChat = i;
    this.selectedHistory = cloneDeep(this.userMessageHistories[this.selectedChat].messages);
    this.selectedHistory.reverse();
  }

  ngOnDestroy(): void {
    this.safeSubscription.forEach(sub => sub.unsubscribe());
  }
}
