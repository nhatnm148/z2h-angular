export class CreateMessageDto {
    from: string;
    to: string;
    message: Message;
}

export class Message {
    content: string;
    sender: string;
}
