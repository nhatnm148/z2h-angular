import { Message } from 'src/modules/messenger/dtos/create-message.dto';

export class User {
    id: string;
    name: string;
    username: string;
}
