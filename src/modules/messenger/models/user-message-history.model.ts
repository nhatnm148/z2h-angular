import { User } from 'src/modules/messenger/models/user.model';
import { Message } from 'src/modules/messenger/dtos/create-message.dto';

export class UserMessageHistory {
    id: string;
    from: User;
    to: User;
    messages: UserMessage[];
}

export class UserMessage extends Message {
    date: string;
}
