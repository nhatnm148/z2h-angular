import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CreateMessageDto } from 'src/modules/messenger/dtos/create-message.dto';
import { Observable } from 'rxjs';
import { UserMessageHistory } from 'src/modules/messenger/models/user-message-history.model';

@Injectable()
export class MessengerService {
    apiUrl = environment.baseUrl + '/messengers';

    constructor(
        private http: HttpClient
    ) { }

    getHistories(userId: string): Observable<UserMessageHistory[]> {
        return this.http.get<UserMessageHistory[]>(`${this.apiUrl}/histories/${userId}`);
    }

    createMessage(payload: CreateMessageDto): Observable<HttpResponse<void>> {
        return this.http.post<void>(`${this.apiUrl}`, payload, { observe: 'response' });
    }
}
