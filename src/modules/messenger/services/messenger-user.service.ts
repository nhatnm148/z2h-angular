import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { User } from 'src/modules/messenger/models/user.model';
import { UserCredentialsDto } from 'src/modules/messenger/dtos/user-credentials.dto';

@Injectable()
export class MessengerUserService {
    apiUrl = environment.baseUrl + '/messenger-users';

    constructor(
        private http: HttpClient
    ) { }

    login(payload: UserCredentialsDto): Observable<HttpResponse<User>> {
        return this.http.post<User>(`${this.apiUrl}/login`, payload, { observe: 'response' });
    }
}
