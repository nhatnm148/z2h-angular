import { Selector } from '@ngxs/store';
import { MessengerState, MessengerStateModel } from 'src/modules/messenger/state/messenger.state';
import { User } from 'src/modules/messenger/models/user.model';
import { UserMessageHistory } from 'src/modules/messenger/models/user-message-history.model';

export class MessengerSelectors {
    @Selector([MessengerState])
    static user$(state: MessengerStateModel): User {
        return state.user;
    }

    @Selector([MessengerState])
    static userMessageHistories$(state: MessengerStateModel): UserMessageHistory[] {
        return state.userMessageHistories;
    }
}
