import { User } from 'src/modules/messenger/models/user.model';
import { UserMessageHistory } from 'src/modules/messenger/models/user-message-history.model';

export class StoreUserAction {
    static readonly type = '[MESSENGER] Store User';

    constructor(public payload: User) { }
}

export class StoreUserMessageHistoriesAction {
    static readonly type = '[MESSENGER] Store User Message Histories';

    constructor(public payload: UserMessageHistory[]) { }
}
