import { User } from 'src/modules/messenger/models/user.model';
import { State, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { StoreUserAction, StoreUserMessageHistoriesAction } from 'src/modules/messenger/state/messenger.actions';
import { UserMessageHistory } from 'src/modules/messenger/models/user-message-history.model';

export interface MessengerStateModel {
    user: User;
    userMessageHistories: UserMessageHistory[];
}

export const initialState: MessengerStateModel = {
    user: null,
    userMessageHistories: []
};

@State({
    name: 'messenger_state',
    defaults: initialState
})
@Injectable()
export class MessengerState {
    @Action(StoreUserAction)
    storeUserAction(context: StateContext<MessengerStateModel>, action: StoreUserAction): void {
        context.patchState({
            user: action.payload
        });
    }

    @Action(StoreUserMessageHistoriesAction)
    storeUserMessageHistoriesAction(
        context: StateContext<MessengerStateModel>,
        action: StoreUserMessageHistoriesAction
    ): void {
        context.patchState({
            userMessageHistories: [ ...action.payload ]
        });
    }
}
